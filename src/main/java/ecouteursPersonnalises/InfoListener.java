package ecouteursPersonnalises;

import composants.ComposantGraphique;

import java.util.EventListener;

/**
 * Classe de listener permettant de transmettre l'information
 *
 * @author Zhaoyi Zhang
 *
 */
public interface InfoListener extends EventListener {
	void transInfo(ComposantGraphique compo);
}
