package aaplication;

import composants.ComposantGraphique;
import utils.SampleIO;

import javax.swing.*;
import java.awt.*;

/**
 * Classe principale de l’inferface d’affichage
 *
 * @author Zhang Zhaoyi
 */
public class App16jeuDUO extends JFrame
{
	/**
	 * Constantes prédéfinies de la taille du
	 * COMPOSANT_GRAPHIQUE et non la JFRAME
	 */
	private final static int
			WIDTH  = 1600,
			HEIGHT = 929;

	/**
	 * Référence du composant graphiique
	 */
	private ComposantGraphique cg;

	/**
	 * Constructeur
	 * Création de la fenêtre
	 */
	private App16jeuDUO() {
		setTitle("JeuDUO — Zhao Yi & Issam Maghni");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		setLayout(null);

		setIconImage(SampleIO.loadImage("icon.png"));

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (final ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ignored) {
			JOptionPane.showMessageDialog(this, "Erreur lors du chargement du LookAndFeel système.", "LookAndFeel introuvable!", JOptionPane.WARNING_MESSAGE);
		}
		System.out.println("Bienvenue dans JeuDUO!");

		cg = new ComposantGraphique(1100, 900);
		cg.toggleAnimation();
		cg.setBounds(0, 0, 1100, 900);
		cg.setFocusable(true);
		add(cg);
	}

	/**
	 * Exécuter le programme
	 *
	 * @param args Liste d’arguments
	 */
	// Issam Maghni
	public static void main(final String... args) {
		EventQueue.invokeLater(() -> {
			try {
				new App16jeuDUO().setVisible(true);
			} catch (final RuntimeException e) {
				e.printStackTrace();
			}
		});
	}
}
