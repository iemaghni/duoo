package map;

import composants.ComposantGraphique;
import interfaces.Drawable;
import utils.Vector;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;

/**
 * Classe de balle lancée par le personnage
 *
 * @author Issam Maghni
 */
public class Ball implements Drawable
{
	private final static double mass = .01, charge = .0001;
	private final static Ellipse2D.Double ell   = new Ellipse2D.Double(0, 0, .2, .2);
	private final static Vector           shift = new Vector(ell.width / 2, ell.height / 2);
	private final        Vector           pos, vit, acc;
	private final Color             color;
	private final Soul              target;
	private final ArrayList<Vector> positions;
	private final Path2D.Double     path;
	private       int               index;

	/**
	 * Constructeur
	 *
	 * @param s Tireur
	 * @param t Cible
	 */
	Ball(Soul s, Soul t, Champ[] champs) {
		color = s.getColor();
		pos = s.getPos().clone();
		vit = s.getVit().clone().multiply(4);
		acc = new Vector();
		target = t;

		path = new Path2D.Double();

		path.moveTo(pos.getX(), pos.getY());

		positions = new ArrayList<>();

		// Encapsuler pour empêcher que ce calcul ne puisse
		// retarder l’intégralité de la boucle du jeu
		new Thread(() -> {
			for (; ; ) {
				acc.reset().sum(Map.G);
				for (final Champ champ : champs)
					acc.sum(champ.calcAcc(this));
				vit.sum(acc.clone().multiply(ComposantGraphique.dt));
				pos.sum(vit.clone().multiply(ComposantGraphique.dt));
				if (!Map.map.cell(pos, true))
					positions.add(pos.clone());
				else break;
			}
		}).start();
	}

	/**
	 * Accesseur de la position
	 *
	 * @return Vecteur position
	 */
	Vector getPos() {
		return pos;
	}

	/**
	 * Accesseur de la vitesse
	 *
	 * @return Vecteur vitesse
	 */
	Vector getVit() {
		return vit;
	}

	/**
	 * Accesseur de la masse
	 *
	 * @return Valeur réelle de la masse
	 */
	double getMasse() {
		return mass;
	}

	/**
	 * Accesseur de la charge
	 *
	 * @return Valeur réelle de la charge
	 */
	double getCharge() {
		return charge;
	}

	/**
	 * Savoir si la balle doit disparaître
	 *
	 * @return Balle doit disparaître
	 */
	boolean isDisposable() {
		return index == positions.size();
	}

	/**
	 * Mettre à jour la positoin et detecter si collision
	 * Ne prend pas de dt car toutes les positions ont déjà
	 * été précalculées dans un thread dans le contructeur
	 */
	void update() {
		if (index < positions.size()) {
			// Si index++, erreur car p.set() will mute
			Vector delta = positions.get(index++).clone().sub(target.getPos());
			if (Math.abs(delta.getX()) < target.shiftX.getX() && Math.abs(delta.getY()) < target.shiftY.getY()) {
				// Sortir de la boucle + Voir isDisposable()
				index = positions.size();
				target.hit();
			}
		}
	}

	@Override
	public void draw(final Graphics2D g2d, final AffineTransform mat) {
		if (index < positions.size()) {
			final Vector p = positions.get(index - 1);
			final double x = p.getX() - shift.getX(), y = p.getY() - shift.getY();
			path.reset();
			path.moveTo(p.getX(), p.getY());
			for (int n = index + 1; n < positions.size(); n++) {
				p.set(positions.get(n));
				path.lineTo(p.getX(), p.getY());
				path.moveTo(p.getX(), p.getY());
			}
			g2d.setColor(color);
			g2d.draw(mat.createTransformedShape(path));
			mat.translate(x, y);
			g2d.fill(mat.createTransformedShape(ell));
			mat.translate(-x, -y);
		}
	}
}
