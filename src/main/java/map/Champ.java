package map;

import interfaces.Drawable;
import utils.Vector;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.io.Serializable;

/**
 * Classe abstraite pour tout champ
 *
 * @author Issam Maghni
 * @author Zhao Yi
 */
abstract class Champ implements Drawable, Serializable
{
	private final static Vector              VOID        = new Vector();
	private final static Color               TRANSPARENT = new Color(0, 0, 0, 0);
	private              Color               color;
	private transient    RadialGradientPaint grad;
	private              Ellipse2D           ell;
	int    rayon;
	Vector pos;

	/**
	 * Constructeur d’un champp générique
	 *
	 * @param x     Position en X
	 * @param y     Position en Y
	 * @param rayon Rayon
	 */
	Champ(int x, int y, int rayon, final Color color) {
		pos = new Vector(x, y);
		setColor(color);
		setRayon(rayon);
		initCircle();
		initGrad();
	}

	/**
	 * Mutateur de la couleur du champ
	 *
	 * @param color Couleur principale du dégradé
	 */
	void setColor(final Color color) {
		this.color = color;
	}

	/**
	 * Mutateur du rayon
	 *
	 * @param rayon Valeur entière
	 */
	void setRayon(int rayon) {
		this.rayon = rayon;
	}

	/**
	 * Méthode qui remodifie la position du Champ
	 *
	 * @param x la position de x
	 * @param y la position de y
	 */
	void setXY(double x, double y) {
		pos.setXY(x, y);
		initCircle();
	}

	/**
	 * Initialiser le cercle à dessiner
	 */
	void initCircle() {
		int r2 = rayon * 2;
		ell = new Ellipse2D.Double(pos.getX() - rayon, pos.getY() - rayon, r2, r2);
	}

	/**
	 * Initialiser le dégragé
	 */
	void initGrad() {
		grad = new RadialGradientPaint(
				(int) (50 * pos.getX()), (int) (50 * pos.getY()), 35 * rayon * 2,
				new float[]{.2f, .8f},
				new Color[]{color, TRANSPARENT},
				MultipleGradientPaint.CycleMethod.NO_CYCLE
		);
	}

	/**
	 * Méthode qui définie le module du champ
	 *
	 * @param module Module du champ
	 */
	abstract void setModule(final double module);

	/**
	 * Méthode qui prend en paramètre la référence
	 * d’une balle et retourne un vecteur force.
	 * DOIT IMPÉRATIVEMENT RETOURNER NULL SI
	 * LA PARTICULE N’EST PAS APPLIQUABLE.
	 *
	 * @param b Référence d’une balle
	 * @return Vecteur force à appliquer
	 */
	abstract Vector calcFor(final Ball b);

	/**
	 * Méthode qui prend en paramètre la référence
	 * d’une balle et retourne un vecteur accélération
	 * selon la masse de la particule ponctuelle.
	 * Le vecteur retourné ne doit pas être altéré!
	 *
	 * @param b Référence d’une balle
	 * @return Vecteur accélération à appliquer
	 */
	Vector calcAcc(final Ball b) {
		// m := Masse de la particule (Kg)
		// F = m⋅a
		// a = F÷m = F*(1/m)
		final Vector force = calcFor(b);
		return force == null ? VOID : force.multiply(1 / b.getMasse());
	}

	@Override
	public void draw(final Graphics2D g2d, final AffineTransform mat) {
		final Paint p = g2d.getPaint();
		g2d.setPaint(grad);
		g2d.fill(mat.createTransformedShape(ell));
		g2d.setPaint(p);
	}

	/**
	 * Champ électrique
	 *
	 * @author Zhao Yi
	 */
	static class Elec extends Champ
	{
		private double charge = -5e-7; // -.0000005

		/**
		 * Constructeur du champ électrique
		 *
		 * @param x     Position en X
		 * @param y     Position en Y
		 * @param rayon Rayon
		 */
		Elec(int x, int y, int rayon) {
			super(x, y, rayon, new Color(255, 0, 0, 155));
		}

		@Override
		void setModule(final double module) {
			charge = module;
		}

		// À recalculer à chaque fois car elle dépend de la distance instantanée
		@Override
		Vector calcFor(final Ball b) {
			// F := Force électrique (N)
			// k := Constante de Coulomb
			// q := Charge qui subit la force électrique (C)
			// Q := Charge qui applique la force électrique (C)
			// r := Distance entre les charges ponctuelles (m)
			// r := Vecteur unitaire orientation de Q (source) à q (cible)
			// F = k(qQ)r^-2.r
			Vector v = b.getPos()
					.clone()
					.sub(pos);
			final double module = v.modulus();
			return module <= rayon
					? v.normalise() // v.multiply(1 / module) // r
					.multiply(9e9) // k
					.multiply(b.getCharge()) // q
					.multiply(charge) // Q
					.multiply(Math.pow(module, -2))
					: null; //r^-2
		}
	}

	/**
	 * Champ magnétique
	 *
	 * @author Issam Maghni
	 */
	static class Magn extends Champ
	{
		private double champMagn = 200;

		/**
		 * Constructeur du champ magnétique
		 *
		 * @param x     Position en X
		 * @param y     Position en Y
		 * @param rayon Rayon
		 */
		Magn(int x, int y, int rayon) {
			super(x, y, rayon, new Color(255, 255, 0, 155));
		}

		@Override
		void setModule(final double module) {
			champMagn = module;
		}

		// À recalculer à chaque fois car elle dépend de la vitesse instantanée
		@Override
		Vector calcFor(final Ball b) {
			// F := Force magnétique (N)
			// q := Charge de la particule (C)
			// v := Vitesse de la particule (m/s)
			// B := Champ magnétique (T)
			// F = q⋅v×B
			return b.getPos().clone().sub(pos).modulus() <= rayon
					? b.getVit().clone().cross(champMagn).multiply(b.getCharge())
					: null;
		}
	}
}
