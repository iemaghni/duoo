package map;

import interfaces.Drawable;
import utils.Vector;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.Serializable;
import java.util.stream.IntStream;

/**
 * Classe de la carte du jeu
 *
 * @author Issam Maghni
 */
public class Map implements Drawable, Serializable
{
	static final  Vector G         = new Vector(0, 9.8);
	public static Matrix map;
	private final Matrix mapInternal;
	private final int    max_balls = 10;
	private final Ball[] balls;
	private final Soul   yin, yang;
	private final Champ[] champs = new Champ[2];

	/**
	 * Constructeur de la map
	 *
	 * @param w Longueur de la map en carraux
	 * @param h Hauteur de la map en carraux
	 */
	public Map(final int w, final int h) {
		mapInternal = new Matrix(w, h);
		map = mapInternal;
		champs[0] = new Champ.Elec(5, 10, 5);
		champs[1] = new Champ.Magn(15, 10, 4);
		addGround(0, h - 1, w);
		addGround(5, 12, 12);
		addGround(0, 8, 5);
		addGround(w - 5, 8, 5);
		addGround(w - 9, 4, 5);
		addGround(4, 4, 5);
		yin = new Soul()
				.setPos(4.5, 3.5)
				.setColor(Color.decode("#6fc4a9"));
		yang = new Soul()
				.setPos(17.5, 3.5)
				.setColor(Color.decode("#f19cb7"));
		balls = new Ball[max_balls];
		init();
	}

	/**
	 * Instancier les objets `transient`, c-a-d.
	 * les références qui ne sont pas `Serializable`,
	 * ou bien ceux qui sont `static`.
	 */
	public void init() {
		yin.loadImage("yin");
		yang.loadImage("yang");
		for (final Champ champ : champs)
			champ.initGrad();
		map = mapInternal;
	}

	/**
	 * Réinitialisation de la map
	 */
	public void reset() {
		// TODO Reset from import?
		// TODO Renaître
		yin.reset();
		yang.reset();
		map.reset();
		clearBalls();
	}

	/**
	 * Enlever toutes les balles
	 */
	public void clearBalls() {
		int n = max_balls;
		while (n-- > 0) balls[n] = null;
	}

	/**
	 * Ajout d’un sol
	 *
	 * @param x Position en X
	 * @param y Position en Y
	 * @param k Nombre de cases
	 */
	private void addGround(int x, final int y, final int k) {
		IntStream.range(x, x + k).parallel().forEach(i -> map.set(i, y, 1, 1));
	}

	/**
	 * Interaction suite à un signal de tir
	 *
	 * @param shooter Tireur
	 * @param target  Cible
	 */
	public void shoot(Soul shooter, Soul target) {
		for (int n = 0; n < 10; n++)
			if (balls[n] == null) {
				balls[n] = new Ball(shooter, target, champs);
				break;
			}
	}

	/**
	 * Personnage #1
	 *
	 * @return Référence
	 */
	public Soul getYin() {
		return yin;
	}

	/**
	 * Personnage #2
	 *
	 * @return Référence
	 */
	public Soul getYang() {
		return yang;
	}

	/**
	 * Rafraîchir les données
	 *
	 * @param dt Valeur de temps infinitésimale
	 */
	public void update(final double dt) {
		yin.update(dt);
		yang.update(dt);
		//Arrays.stream(balls).filter(Objects::nonNull).parallel().forEach(ball -> {});
		for (int n = 0; n < 10; n++) {
			if (balls[n] != null) {
				balls[n].update();
				if (balls[n].isDisposable())
					balls[n] = null;
			}
		}
		// Gestion de vie
		if (yin.getLife() * yang.getLife() == 0) {
			JOptionPane.showMessageDialog(null, String.format("Le personnage %s a gagné", yin.getLife() > 0 ? "Yin (vert)" : "Yang (rouge)"));
			clearBalls();
			yin.reset();
			yang.reset();
		}
	}

	/**
	 * Dessin de la map
	 *
	 * @param g2d Graphic2D dans lequel le dessin se fera
	 * @param mat Matrice de dessin qui gère les proportions
	 */
	@Override
	public void draw(final Graphics2D g2d, final AffineTransform mat) {
		yin.draw(g2d, mat);
		yang.draw(g2d, mat);
		for (final Champ champ : champs)
			champ.draw(g2d, mat);
		for (Ball ball : balls)
			if (ball != null)
				ball.draw(g2d, mat);
	}

	/**
	 * setter de la position de yin
	 *
	 * @param x Position en X
	 * @param y Position en Y
	 */
	public void setYin(int x, int y) {
		yin.setPos(x + .5, y + .5);
		yin.getVit().reset();
	}

	/**
	 * setter de la position de yang
	 *
	 * @param x Position en X
	 * @param y Position en Y
	 */
	public void setYang(int x, int y) {
		yang.setPos(x + .5, y + .5);
		yang.getVit().reset();
	}

	/**
	 * Méthode qui définie la position planaire du champ
	 *
	 * @param n Indice du champ
	 * @param x Position en X
	 * @param y Position en Y
	 */
	// Zhaoyi Zhang
	public void setPosChamp(int n, double x, double y) {
		champs[n].setXY(x, y);
		champs[n].initGrad();
	}

	/**
	 * méthode qui change la gravité
	 *
	 * @param d la valeur de l'accélération gravitationnelle
	 */
	public static void setG(double d) {
		G.setY(d);
	}

	/**
	 * Méthode qui définie le module du champ
	 *
	 * @param n      Indice du champ
	 * @param module Nouvelle valeur du module
	 */
	// Zhaoyi Zhang
	public void setModChamp(int n, double module) {
		champs[n].setModule(module);
	}

	/**
	 * Méthode qui définie le rayon du champ
	 *
	 * @param n     Indice du champ
	 * @param rayon la valeur de rayon
	 */
	public void setRayChamp(int n, int rayon) {
		champs[n].setRayon(rayon);
		champs[n].initCircle();
		champs[n].initGrad();
	}

	/**
	 * @author Issam Maghni
	 */
	public static class Matrix implements Serializable
	{ // Immuable
		final int width, height;
		private final int[][] mat;

		/**
		 * Constructeur
		 *
		 * @param width  Longueur/largeur de la matrice
		 * @param height Hauteur de la matrice
		 */
		Matrix(final int width, final int height) {
			this.width = width;
			this.height = height;
			mat = new int[height][width];
		}

		// Tests unitaires des cas particuliers
		public static void main(final String... args) {
			final Matrix mat = new Matrix(10, 5);
			System.out.println(!mat.cell(new Vector(0, 0), false));
			System.out.println(!mat.cell(new Vector(5, -1), true));
			System.out.println(!mat.cell(new Vector(-1, 0), false));
			System.out.println(mat.cell(new Vector(-1, 0), true));
			System.out.println(mat.cell(new Vector(-1, -1), true));
			System.out.println(!mat.cell(new Vector(-1, -1), false));
			System.out.println(mat.cell(new Vector(0, 5), false));
			System.out.println(mat.cell(new Vector(0, 10), false));
		}

		/**
		 * Vider la matrice
		 */
		void reset() {
			for (int w = width; w-- > 0; )
				for (int h = height; h-- > 0; )
					set(w, h, 0, 0);
		}

		/**
		 * Ajout d’un bloc selon un type
		 *
		 * @param x      Position en X
		 * @param y      Position en Y
		 * @param type   Type de case
		 * @param altype Type d’alternance
		 * @return Valeur de la position
		 */
		public int set(final int x, final int y, final int type, final int altype) {
			final int val = get(x, y) == type ? altype : type;
			mat[y][x] = val;
			return val;
		}

		/**
		 * Acccesseur de la matrice au complet
		 *
		 * @return Matrice
		 */
		public int[][] get() {
			return mat;
		}

		/**
		 * Type de la case
		 *
		 * @param x Position en X
		 * @param y Position en Y
		 * @return Type numérique
		 */
		int get(final int x, final int y) {
			return mat[y][x];
		}

		/**
		 * Méthode qui permet de s’assurer
		 * que la cases est bel-et-bien vide.
		 *
		 * @param x Position en X
		 * @param y Position en Y
		 * @return Si la case est vide
		 */
		public boolean isSafe(final int x, final int y) {
			return get(x, y) == 0;
		}

		/**
		 * Case n’est non-vide
		 *
		 * @param x             Position en X
		 * @param y             Position en Y
		 * @param limitOverflow Ne doit pas déborder
		 * @return Case non-vide
		 */
		public boolean tile(final double x, final double y, final boolean limitOverflow) {
			// TODO Simplify
			// The implementation should satisfy the non-exhaustive tests
			// which *should* handle pretty much all the sketchy cases
			if (limitOverflow)
				if (x < 0 || x >= width || y >= height)
					return true;
			if (y >= height)
				return true;
			if (y < 0 || x < 0 || x >= width)
				return false;
			return get((int) x, (int) y) > 0;
		}

		/**
		 * Vérifier si la case courante est occupée
		 *
		 * @param v             Vecteur position
		 * @param limitOverflow Ne doit pas déborder
		 * @return Si case occupée
		 */
		boolean cell(final Vector v, final boolean limitOverflow) {
			return tile(v.getX(), v.getY(), limitOverflow);
		}
	}
}
