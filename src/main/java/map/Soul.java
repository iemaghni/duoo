package map;

import utils.SampleIO;
import utils.Vector;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.io.Serializable;

/**
 * Classe de personnages
 * Images doivent IMPÉRATIVEMENT posséder une hauteur de 50px.
 *
 * @author Issam Maghni
 * @author Zhao Yi
 */
// `Composition over inheritance` philosophy
public class Soul implements Serializable
{
	private static double impulse = 10;
	private final  Vector posInit = new Vector(), pos = new Vector(), vit = new Vector(), acc = new Vector();
	final Vector shiftX = new Vector(), shiftY = new Vector(0, .5);
	private transient Image[] sprites; // Images tampons
	private transient Image   current, front, jump, hud;
	private Color         color;
	private Line2D.Double fleche = new Line2D.Double();
	private int           life   = 100;
	private int           index, hash;
	private double counter;

	/**
	 * Restreindre une valeur entre zero et une limite
	 *
	 * @param val   Valeur à restreindre
	 * @param limit Valeur maximale tolérée
	 * @return Valeur à l’intérieur de la limite
	 */
	public static double limit(double val, double limit) {
		if (val < 0)
			return limit - val;
		if (val > limit)
			return val - limit;
		return val;
	}

	/**
	 * Issam Maghni
	 * Accesseur de la position
	 * NE PAS MODIFIER LES VALEURS DU VECTEUR
	 *
	 * @return Position vectorielle
	 */
	Vector getPos() {
		return pos;
	}

	/**
	 * Issam Maghni
	 * Mutateur de la position
	 *
	 * @param x Position en X
	 * @param y Position en Y
	 * @return Soi-même
	 */
	Soul setPos(double x, double y) {
		posInit.setXY(x, y);
		pos.set(posInit);
		return this;
	}

	/**
	 * Issam Maghni
	 * Accesseur de la vitesse
	 *
	 * @return Vitesse vectorielle
	 */
	public Vector getVit() {
		return vit;
	}

	public Vector getAcc() {
		return acc;
	}

	/**
	 * Issam Maghni
	 * Retourne la couleur propre
	 *
	 * @return Couleur
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Issam Maghni
	 * Mutateur de la couleur
	 *
	 * @param color Couleur du personnage
	 * @return Soi-même
	 */
	Soul setColor(final Color color) {
		hash = color.hashCode();
		this.color = color;
		return this;
	}

	/**
	 * Issam Maghni
	 * Obtenir un hash de l’objet selon sa couleur propre
	 *
	 * @return Hash entier
	 */
	@Override
	public int hashCode() {
		return hash;
	}

	/**
	 * Issam Maghni
	 * Réinitialisation des vecteurs à zéro
	 */
	public void reset() {
		pos.set(posInit);
		vit.reset();
		acc.reset();
		life = 100;
	}

	/**
	 * Issam Maghni
	 * Charger l’image
	 *
	 * @param pathname Chemin des images
	 */
	void loadImage(final String pathname) {
		sprites = new Image[11];
		for (int n = 0; n < sprites.length; n++)
			sprites[n] = SampleIO.loadImage(pathname + "/" + String.format("%02d", n + 1) + ".png");
		hud = SampleIO.loadImage(pathname + "/hud.png");
		jump = SampleIO.loadImage(pathname + "/jump.png");
		front = SampleIO.loadImage(pathname + "/front.png");
		current = front;
		shiftX.setX(current.getWidth(null) / (2d * 50));
	}

	/**
	 * Issam Maghni
	 * Obtenir l’image identitaire
	 *
	 * @return Référence polymorphique de l’image
	 */
	public Image getImage() {
		return hud;
	}

	/**
	 * Issam Maghni
	 * Obtenir le pointage de vie
	 *
	 * @return Valeur entrière comprise entre -∞, 100]
	 */
	public int getLife() {
		return life;
	}

	/**
	 * Issam Maghni
	 * Appliquer un coup sur le personnage
	 */
	void hit() {
		if (life > 0) life -= 10;
	}

	/**
	 * Issam Maghni
	 * Mettre à jour
	 *
	 * @param dt Valeur du temps d’écouelement
	 */
	public void update(final double dt) {
		acc.setY(Map.G.getY());

		if (vit.getY() >= 0) { // Chûte ou sur le sol
			if (Map.map.cell(pos.clone().sum(shiftY), false)) {
				acc.setY(0);
				vit.setY(0);
			}
		} else { // Dans les airs
			if (Map.map.cell(pos.clone().sub(shiftY), false)) {
				vit.setY(0);
			}
		}

		// Possible to one clone and two sum
		// But that’s not idiomatic at all.
		// Collision sur les côtés
		if (vit.getX() < 0 && Map.map.cell(pos.clone().sub(shiftX), false)
				|| (vit.getX() > 0 && Map.map.cell(pos.clone().sum(shiftX), false))) {
			acc.setX(0);
			vit.setX(-.4 * vit.getX());
		}

		vit.sum(acc.clone().multiply(dt));
		final Vector dx = vit.clone().multiply(dt);
		pos.sum(dx);

		if (acc.getX() == 0) {
			double v = vit.getX();
			if (v < 0)
				vit.setX((v += .01) < 0 ? v : 0);
			else if (v > 0)
				vit.setX((v -= .01) > 0 ? v : 0);
		}

		// Itérer les images
		if (dx.getX() == 0)
			current = front;
		else if (dx.getY() == 0) {
			counter += Math.abs(dx.getX());
			if (counter > .25) {
				counter = 0;
				current = this.sprites[index++];
				index %= sprites.length;
			}
		} else current = jump;

		// Faire en sorte que le personnage se téléporte sur les bordures
		pos.setX(limit(pos.getX(), Map.map.width));
	}

	/**
	 * Zhao Yi
	 * Dessin
	 *
	 * @param g2d Graphic2D dans lequel le dessin se fera
	 * @param mat Matrice de dessin qui gère les proportions
	 */
	public void draw(final Graphics2D g2d, final AffineTransform mat) {
		// Positioner selon le personnage
		//AffineTransform mat = new AffineTransform(mat0);
		mat.translate(pos.getX(), pos.getY());

		// Positioner la flèche
		fleche.x2 = vit.getX() / 5;
		fleche.y2 = vit.getY() / 5;
		// Dessiner la flèche avec la couleur du personnage
		g2d.setColor(color);
		g2d.draw(mat.createTransformedShape(fleche));

		// Évaluer la nécessité de miroiter
		boolean flip = vit.getX() < 0;
		// Miroiter si nécessaire
		// SACHEZ QU'IL EST IMPÉRATIF DE MIROITER
		// *AVANT* DE PROCÉDER À TOUT DÉCALLAGE.
		if (flip) mat.scale(-1, 1);
		// Centrer l’image
		mat.translate(-shiftX.getX(), -shiftY.getY());
		// Raptisser
		mat.scale(.02, .02);
		// Dessiner l’image
		g2d.drawImage(current, mat, null);
		// Annuler le miroitage s’il y a eu lieu
		if (flip) mat.scale(-1, 1);
		// Annuler le centrage de l’image et le positionnement
		mat.translate(-mat.getTranslateX(), -mat.getTranslateY());
		// Agrandir
		mat.scale(50, 50);
	}

	public void up() {
		if (acc.getY() == 0)
			vit.setY(-impulse);
	}

	/**
	 * Déplacement vers la droite
	 *
	 * @param isPressed Touche préssée ou relâchée?
	 */
	public void right(final boolean isPressed) {
		acc.setX(isPressed ? impulse : 0);
	}

	/**
	 * Déplacement vers la gauche
	 *
	 * @param isPressed Touche préssée ou relâchée?
	 */
	public void left(final boolean isPressed) {
		acc.setX(isPressed ? -impulse : 0);
	}
}

/*
Image img = new BufferedImage(50, 50, BufferedImage.TYPE_INT_ARGB);
Graphics2D g = (Graphics2D) img.getGraphics();
g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
g.drawImage(this.img, null, null);
g.isDisposable();
this.img = img;
*/
