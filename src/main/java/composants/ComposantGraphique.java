package composants;

import ecouteursPersonnalises.InfoListener;
import map.Map;
import map.Soul;
import utils.Key;
import utils.ModeleAffichage;
import utils.SampleIO;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

/**
 * Classe qui gère l’affichage de la case du jeu
 *
 * @author Zhaoyi Zhang
 * @author Issam Maghni
 */
public class ComposantGraphique extends JPanel implements Runnable
{
	public final static  double dt        = .001;
	public final static  int    iteration = 10;
	private final static Image  GROUND    = SampleIO.loadImage("ground.png");
	private final        int    SIZE      = 50;
	private final        double RATIO     = 1d / SIZE;
	/**
	 * Dimensions en blocs ou mètres
	 */
	private final        int    w, h, H, W;
	private final EventListenerList OBJETS_ENREGISTRES = new EventListenerList();
	private final Color             background, filterColor;
	private final Rectangle filterShape;
	private final Image     logo;
	private final int       INIT = 0, IDLE = 1, PLAY = 2, EDIT = 3;
	private final ModeleAffichage    mod;
	private final Rectangle2D.Double rectLife;
	private       Component          statusAjout = Component.TILE;
	private       AffineTransform    mat;
	private       Shape              grid;
	private       Map                map;
	private       int                status;
	private       Graphics2D         g2d;
	private       Image              img;
	private       boolean            premierClick;

	/**
	 * setter de statusAjout
	 *
	 * @param statusAjout determiner si on ajoute un bloc (statusAjout=0), ou on crée le point de départ des personnage (statusAjout=1 , 2)
	 */
	// Zhaoyi Zhang
	public void setStatusAjout(Component statusAjout) {
		this.statusAjout = statusAjout;
	}

	/**
	 * Constructeur
	 * stackoverflow.com/a/7471183
	 *
	 * @param W largeur du composant
	 * @param H hauteur du composant
	 */
	// Issam Maghni
	public ComposantGraphique(final int W, final int H) {
		this.H = H;
		this.W = W;
		w = W / SIZE;
		h = H / SIZE - 1; // Last row is to exclude
		mod = new ModeleAffichage(W, H, w);
		filterShape = new Rectangle(0, 0, W, H);
		filterColor = new Color(0, 0, 0, 80);
		background = Color.decode("#82ccdd");
		logo = SampleIO.loadImage("logo.png");
		rectLife = new Rectangle2D.Double(0, H - SIZE, 0, SIZE);
		map = new Map(w, h);

		setName("Composant graphique");
		setLayout(null);
		setSize(W, H);

		initMat();
		initGrid();

		manageKeys();
		manageMouse();
	}

	/**
	 * Issam Maghni
	 * Charger une sauvegarde
	 *
	 * @param file Fichier depuis lequel la map sera chargée
	 * @return Si le chargement est réussi
	 */
	// Issam Maghni
	public boolean loadMap(File file) {
		Map localMap;
		try {
			localMap = SampleIO.loadDataFromRawFile(file);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		boolean notNull = localMap != null;
		setMap(notNull ? localMap : new Map(w - 1, h));
		repaint();
		return notNull;
	}

	/**
	 * Permet la sauvegarde du la map courante
	 *
	 * @param pathname Chemin et nom de sauvegarde
	 * @return Si l’écriture s’est déroulé correctement
	 */
	// Issam Maghni
	public boolean saveMap(String pathname) {
		try {
			SampleIO.saveDataToRawFile(pathname.concat(".map"), map);
			return true;
		} catch (final IOException ignored) {
			return false;
		}
	}

	/**
	 * En cours d’exécution…
	 *
	 * @return Valeur
	 */
	// Issam Maghni
	public boolean isRunning() {
		return status == PLAY;
	}

	/**
	 * Instancier la matrice transformée de ce composant
	 */
	// Issam Maghni
	private void initMat() {
		mat = mod.getMat();
	}

	/**
	 * Initialiser la map au complet et
	 * la dessiner dans une image fixe.
	 */
	// Issam Maghni
	private void initGraphicalMap() {
		g2d.setColor(background);
		g2d.fillRect(0, 0, getWidth(), getHeight());
		int[][] map = Map.map.get();
		// Offset to handle the invisible border
		mat.scale(RATIO, RATIO);
		for (final int[] row : map) {
			for (final int i : row) {
				if (i == 1)
					g2d.drawImage(GROUND, mat, null);
				mat.translate(SIZE, 0);
			}
			mat.translate(-row.length * SIZE, SIZE);
		}
		// Unshift the offset
		mat.translate(0, -map.length * SIZE);
		mat.scale(SIZE, SIZE);
	}

	/**
	 * Initialiser la grille d’édition
	 */
	// Issam Maghni
	private void initGrid() {
		Path2D.Double grid = new Path2D.Double();
		for (int x = w; x-- > 0; ) {
			grid.moveTo(x, 0);
			grid.lineTo(x, h);
		}
		for (int y = h; y-- > 0; ) {
			grid.moveTo(0, y);
			grid.lineTo(w, y);
		}
		this.grid = mod.getMat().createTransformedShape(grid);
	}

	/**
	 * Accesseur du personnage principal
	 *
	 * @return Référence au personnage primaire
	 */
	// Issam Maghni
	public Soul getYin() {
		return getMap().getYin();
	}

	/**
	 * Accesseur du personnage secondaire
	 *
	 * @return Référence au second personnage
	 */
	// Issam Maghni
	public Soul getYang() {
		return getMap().getYang();
	}

	/**
	 * Basculer entre play/pause
	 */
	// Issam Maghni
	public void toggleAnimation() {
		if (status != PLAY) {
			status = PLAY;
			new Thread(this).start();
		} else {
			status = INIT;
			repaint();
		}
	}

	/**
	 * Alternance du mode édition
	 */
	// Issam Maghni
	public void toggleEditor() {
		if (status != EDIT) {
			status = EDIT;
			map.clearBalls();
			repaint();
		}
	}

	/**
	 * Méthode de dessin d’une case à la fois
	 *
	 * @param x      Position en X
	 * @param y      Position en Y
	 * @param type   Type à ajouter
	 * @param altype Type de recours
	 */
	// Issam Maghni
	private void add(int x, int y, int type, int altype) {
		final int effective = Map.map.set(x, y, type, altype),
				xPixels = x * SIZE, yPixels = y * SIZE;
		mat.translate(x, y);
		mat.scale(RATIO, RATIO);
		if (effective == 1)
			g2d.drawImage(GROUND, mat, null);
		else
			g2d.clearRect(xPixels, yPixels, SIZE, SIZE);
		mat.translate(-mat.getTranslateX(), -mat.getTranslateY());
		mat.scale(SIZE, SIZE);
		repaint(xPixels, yPixels, xPixels + SIZE, yPixels + SIZE);
	}

	/**
	 * Méthode qui permet d’effacer toutes les cases voisines
	 *
	 * @param x Position en X
	 * @param y Position en Y
	 */
	// Issam Maghni
	private void del(int x, int y) {
		if (y < h && Map.map.tile(x, y, false)) {
			add(x, y, 0, 0);
			del(x - 1, y);
			del(x + 1, y);
			del(x, y - 1);
			del(x, y + 1);
		}
	}

	/**
	 * les gestions de souris permettantes d'éditer le map
	 */
	// Zhaoyi Zhang
	private void manageMouse() {
		addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e) {
				if (status == EDIT) {
					int x = e.getX() / SIZE, y = e.getY() / SIZE;
					premierClick = Map.map.isSafe(x, y);
					switch (statusAjout) {
						case TILE:
							add(x, y, 1, 0);
							break;
						case DEL:
							del(x, y);
							break;
						case YIN:
							if (Map.map.isSafe(x, y)) {
								map.setYin(x, y);
								repaint();
							}
							break;
						case YANG:
							if (Map.map.isSafe(x, y)) {
								map.setYang(x, y);
								repaint();
							}
							break;
						case CHAMP_ELEC:
							map.setPosChamp(0, (double) e.getX() / SIZE, (double) e.getY() / SIZE);
							repaint();
							break;
						case CHAMP_MAGN:
							map.setPosChamp(1, (double) e.getX() / SIZE, (double) e.getY() / SIZE);
							repaint();
							break;
					}
				}
			}
		});

		addMouseMotionListener(new MouseMotionAdapter()
		{
			public void mouseDragged(MouseEvent e) {
				if (status == EDIT) {
					int x = e.getX() / SIZE, y = e.getY() / SIZE;
					switch (statusAjout) {
						case TILE:
							if (premierClick)
								add(x, y, 1, 1);
							else
								add(x, y, 0, 0);
							break;
						case YIN:
							if (Map.map.isSafe(x, y))
								map.setYin(x, y);
							break;
						case YANG:
							if (Map.map.isSafe(x, y))
								map.setYang(x, y);
							break;
						case CHAMP_ELEC:
							map.setPosChamp(0, (double) e.getX() / SIZE, (double) e.getY() / SIZE);
							repaint();
							break;
						case CHAMP_MAGN:
							map.setPosChamp(1, (double) e.getX() / SIZE, (double) e.getY() / SIZE);
							repaint();
							break;
					}
				} else if (status == IDLE) {
					for (int n = iteration; n-- > 0; )
						map.update(dt);
					repaint();
				}
			}
		});
	}

	/**
	 * Gestion centralisée du clavier
	 */
	// Issam Maghni
	private void manageKeys() {
		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), Key.UP);
		getActionMap().put(Key.UP, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYin().up();
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_W, 0), Key.ALT_UP);
		getActionMap().put(Key.ALT_UP, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYang().up();
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), Key.LEFT);
		getActionMap().put(Key.LEFT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYin().left(true);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, true), Key.NO_LEFT);
		getActionMap().put(Key.NO_LEFT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYin().left(false);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_A, 0), Key.ALT_LEFT);
		getActionMap().put(Key.ALT_LEFT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYang().left(true);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_A, 0, true), Key.NO_ALT_LEFT);
		getActionMap().put(Key.NO_ALT_LEFT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYang().left(false);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), Key.RIGHT);
		getActionMap().put(Key.RIGHT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYin().right(true);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, true), Key.NO_RIGHT);
		getActionMap().put(Key.NO_RIGHT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYin().right(false);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_D, 0), Key.ALT_RIGHT);
		getActionMap().put(Key.ALT_RIGHT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYang().right(true);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_D, 0, true), Key.NO_ALT_RIGHT);
		getActionMap().put(Key.NO_ALT_RIGHT, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				getYang().right(false);
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, true), KeyEvent.VK_DOWN);
		getActionMap().put(KeyEvent.VK_DOWN, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				map.shoot(getYin(), getYang());
			}
		});

		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_S, 0, true), KeyEvent.VK_S);
		getActionMap().put(KeyEvent.VK_S, new AbstractAction()
		{
			@Override
			public void actionPerformed(final ActionEvent actionEvent) {
				map.shoot(getYang(), getYin());
			}
		});
	}

	/**
	 * Méthode qui dessine la map.
	 *
	 * @param g Graphics
	 */
	// Issam Maghni
	@Override
	public void paintComponent(final Graphics g) {
		super.paintComponent(g);

		if (img == null) {
			img = createImage(getWidth(), getHeight() - SIZE);
			g2d = (Graphics2D) img.getGraphics();
			g2d.setBackground(background);
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			initGraphicalMap();
		}

		final Graphics2D g2d = (Graphics2D) g;

		// Définir l’épaisseur du tracé
		g2d.setStroke(new BasicStroke(1));

		// Dessin du fond (plateforme)
		g2d.drawImage(img, null, null);

		// Appliquer le lissage
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Échelle de gradation
		for (int n = w; n-- > 0; )
			g2d.drawString(n + "m", n * SIZE, H - SIZE);

		// Dessiner les personnages
		map.draw(g2d, mat);

		// Dessiner les barres de vie
		g2d.setColor(getYin().getColor());
		rectLife.x = 0;
		rectLife.width = getYin().getLife() * .25 * w;
		g2d.fill(rectLife);
		g2d.drawImage(getYin().getImage(), 0, H - SIZE, null);
		g2d.setColor(getYang().getColor());
		rectLife.width = getYang().getLife() * .25 * w;
		rectLife.x = W - rectLife.width;
		g2d.fill(rectLife);
		g2d.drawImage(getYang().getImage(), W - 50, H - SIZE, null);

		switch (status) {
			case INIT: // Si à l’état initial
				g2d.drawImage(logo, W / 2 - 255, H / 2 - 60, null);
			case IDLE: // Si en mode initial ou en attente
				g2d.setColor(filterColor);
				g2d.fill(filterShape);
				break;
			case EDIT: // Si en mode éditeur
				g2d.setColor(Color.BLACK);
				g2d.draw(grid);
		}

		// Si en mode éditeur
		/*if (status == EDIT) {
			g2d.setColor(Color.BLACK);
			g2d.draw(grid);
		}

		// Si en mode initial ou en attente
		if (status < PLAY) {
			g2d.setColor(filterColor);
			g2d.fill(filterShape);
		}

		// Si à l’état initial
		if (status == INIT) {
			g2d.drawImage(logo, W / 2 - 255, H / 2 - 60, null);
		}*/
	}

	/**
	 * Boucle d’animation
	 */
	// Issam Maghni
	@Override
	public void run() {
		while (status == PLAY) {
			long time = System.currentTimeMillis();
			for (int n = iteration; n-- > 0; )
				map.update(dt);
			repaint();
			leverEvenInfo();
			time = System.currentTimeMillis() - time;
			if (time < 15) {
				try {
					Thread.sleep(15 - time);
				} catch (final InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	/**
	 * méthode qui permet dajouter un ecouteur d’information
	 *
	 * @param listener objet qui ecoute l’ordre
	 */
	// Zhaoyi Zhang
	public void addInfoListener(InfoListener listener) {
		OBJETS_ENREGISTRES.add(InfoListener.class, listener);
	}

	/**
	 * méthode qui envoie l’information, celle-ci fonctionne pendant que le composant est en mouvement
	 */
	// Zhaoyi Zhang
	private void leverEvenInfo() {
		for (InfoListener listener : OBJETS_ENREGISTRES.getListeners(InfoListener.class))
			listener.transInfo(this);
	}

	/**
	 * Méthode qui retourne le map de composant graphique
	 *
	 * @return le map de composant graphique
	 */
	// Zhaoyi Zhang
	public Map getMap() {
		return map;
	}

	/**
	 * Méthode qui modifie ou crée le map de composant graphique
	 *
	 * @param map de composant graphique
	 */
	// Zhaoyi Zhang
	public void setMap(Map map) {
		this.map = map;
		map.init();
		initGraphicalMap();
	}

	/**
	 * Méthode qui réinitialise le composant graphique
	 */
	// Zhaoyi Zhang
	public void reset() {
		map.reset();
		initGraphicalMap();
		status = IDLE;
		repaint();
	}

	/**
	 * Méthode qui définie la charge du champ magnétique
	 *
	 * @param module Nouvelle valeur de la charge en nC
	 */
	// Zhaoyi Zhang
	public void setModuleChampElec(double module) {
		map.setModChamp(0, module / 1e9);
	}

	/**
	 * Méthode qui définit le rayon de Champ électrique
	 *
	 * @param rayon Nouvelle valeur du rayon
	 */
	// Zhaoyi Zhang
	public void setRayonChampElec(int rayon) {
		map.setRayChamp(0, rayon);
		repaint();
	}

	/**
	 * Méthode qui définie le module du champ magnétique
	 *
	 * @param module la nouvelle valeur de module
	 */
	// Zhaoyi Zhang
	public void setModuleChampMagn(double module) {
		map.setModChamp(1, module);
	}

	/**
	 * Méthode qui définie le rayon du champ magnétique
	 *
	 * @param rayon la nouvelle longueur de champ
	 */
	// Zhaoyi Zhang
	public void setRayonChampMagn(int rayon) {
		map.setRayChamp(1, rayon);
		repaint();
	}
}

