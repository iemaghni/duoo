package utils;

public enum Key
{
	UP, LEFT, RIGHT,
	ALT_UP, ALT_LEFT, ALT_RIGHT,
	NO_LEFT, NO_RIGHT,
	NO_ALT_LEFT, NO_ALT_RIGHT
}
