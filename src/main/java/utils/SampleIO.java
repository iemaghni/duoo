package utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.invoke.MethodHandles;

/**
 * Classe d’interaction Input/Output
 *
 * @author Issam Maghni
 */
@SuppressWarnings("unused")
public class SampleIO
{
	/**
	 * Méthode de chargement depuis un fichier brute externe
	 *
	 * @param pathname Chemin du fichier à charger
	 * @param <T>      Type d’inférence
	 * @return Référence de l’objet chargé
	 * @throws IOException            Erreur lors de la lecture
	 * @throws ClassNotFoundException Erreur lors du cast car la classe est introuvable
	 */
	public static <T> T loadDataFromRawFile(final String pathname) throws IOException, ClassNotFoundException {
		return loadDataFromRawFile(new File(pathname));
	}

	/**
	 * Méthode de chargement depuis un fichier brute externe
	 *
	 * @param file Fichier à charger
	 * @param <T>  Type d’inférence
	 * @return Référence de l’objet chargé
	 * @throws IOException            Erreur lors de la lecture
	 * @throws ClassNotFoundException Erreur lors du cast car la classe est introuvable
	 */
	@SuppressWarnings("unchecked")
	public static <T> T loadDataFromRawFile(final File file) throws IOException, ClassNotFoundException {
		T obj;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
			obj = (T) ois.readObject();
		}
		return obj;
	}

	/**
	 * Méthode d’enregistrement vers un fichier brute externe
	 *
	 * @param pathname Chemin
	 * @param obj      Référence de l’objet de sauvegarde
	 * @param <T>      Type d’inférence
	 * @throws IOException Erreur lors de la lecture
	 */
	public static <T> void saveDataToRawFile(final String pathname, final T obj) throws IOException {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(pathname))) {
			oos.writeObject(obj);
		}
	}

	/**
	 * Chargement d’image.
	 *
	 * @param pathname Chemin
	 * @return BufferedImage ou null
	 */
	public static BufferedImage loadImage(String pathname) {
		BufferedImage bufImg = null;
		try {
			// stackoverflow.com/a/29477034
			bufImg = ImageIO.read(MethodHandles.lookup().lookupClass().getClassLoader().getResourceAsStream(pathname));
		} catch (final IllegalArgumentException | IOException ignored) {
			System.err.printf("`%s` probablement introuvable!\n", pathname);
		}
		return bufImg;
	}
}
