package utils;

import javax.swing.*;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Implémentation naïve et primitive d’une
 * redirection de sortie vers un JTextArea.
 *
 * @author Issam Maghni
 */
public class PipePrompt extends OutputStream
{
	private final JTextArea         textArea;
	private final PipedOutputStream out;
	private       Reader            reader;

	/**
	 * Constructeur
	 *
	 * @param textArea Référence d’un JTextArea de sortie
	 */
	public PipePrompt(final JTextArea textArea) {
		this.textArea = textArea;
		out = new PipedOutputStream();
		try {
			reader = new InputStreamReader(new PipedInputStream(out), StandardCharsets.UTF_8);
		} catch (final UnsupportedEncodingException ignored) {
			System.out.println("Encodage UTF8 non-supporté par votre plateforme!");
		} catch (final IOException ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	/**
	 * Méthode d’écriture
	 *
	 * @param i Valeur du caractère à écrire dans le tampon
	 * @throws IOException Erreur possible lors de l’écriture
	 */
	public void write(final int i) throws IOException {
		out.write(i);
	}

	/**
	 * Méthode de transverse du tempon vers le JTextArea
	 *
	 * @throws IOException Erreur possible lors du transfert
	 */
	public void flush() throws IOException {
		if (reader.ready()) {
			final char[] chars = new char[1024];
			textArea.append(new String(chars, 0, reader.read(chars)));
		}
	}
}
