package utils;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.Serializable;

/**
 * Un objet ModeleAffichage permet de mémoriser un ensemble de valeurs pour passer du monde réel vers un composant de dessin dont les
 * coordonnées sont en pixels. On peut interroger l’objet pour connaitre la matrice de transformation, le nombre de pixels par unité,
 * les dimensions dans le monde réel, etc.
 *
 * @author Caroline Houle
 * @author Issam Maghni
 */

public class ModeleAffichage implements Serializable
{
	private final AffineTransform mat = new AffineTransform(); // Matrice identité
	private       double
	                              xOrigUnitesReelle, yOrigUnitesReelle,
			largUnitesReelles, hautUnitesReelles,
			largPixels, hautPixels,
			pixelsParUniteX, pixelsParUniteY;

	/**
	 * Permet de créer un objet ModelAffichage, pouvant mémoriser la matrice (et autres valeurs) de transformation pour passer du monde vers le composant.
	 * Les dimensions du monde réel sont passées en paramètre (largeur et hauteur).
	 * Il y a distortion si le rapport entre les dimensions en pixels n’est pas identique au rapport entre les dimensions réelles.
	 *
	 * @param largPixels        La largeur du composant, en pixels
	 * @param hautPixels        La hauteur du composant, en pixels
	 * @param xOrigUnitesReelle L’origine en x de la portion du monde réel que l’on veut montrer
	 * @param yOrigUnitesReelle L’origine en y de la portion du monde réel que l’on veut montrer
	 * @param largUnitesReelles La largeur considérée dans le monde, en unité réelles
	 * @param hautUnitesReelles La hauteur considérée dans le monde, en unité réelles
	 */
	public ModeleAffichage(
			final double largPixels, final double hautPixels,
			final double xOrigUnitesReelle, final double yOrigUnitesReelle,
			final double largUnitesReelles, final double hautUnitesReelles) {

		this.largPixels = largPixels;
		this.hautPixels = hautPixels;
		this.xOrigUnitesReelle = xOrigUnitesReelle;
		this.yOrigUnitesReelle = yOrigUnitesReelle;
		this.largUnitesReelles = largUnitesReelles;
		this.hautUnitesReelles = hautUnitesReelles;

		// Calcul de la matrice monde-vers-composant
		compute();
	}

	/**
	 * Permet de créer un objet ModeleAffichage, pouvant mémoriser la matrice (et autres valeurs) de transformation pour passer du monde vers le composant.
	 * Une des dimensions du monde réel est passée en paramètre (largeur ou hauteur).
	 * L’autre dimension sera calculée de façon à n’introduire aucune distortion.
	 *
	 * @param largPixels               La largeur du composant, en pixels
	 * @param hautPixels               La hauteur du composant, en pixels
	 * @param xOrigUnitesReelle        L’origine en x de la portion du monde réel que l’on veut montrer
	 * @param yOrigUnitesReelle        L’origine en y de la portion du monde réel que l’on veut montrer
	 * @param dimensionEnUnitesReelles La dimensions considérée dans le monde, en unité réelles (il peut s’agir d’une largeur ou d’une hauteur, dépendant du dernier paramètre)
	 * @param typeLargeur              Booléen qui vaut vrai si la dimension fournie est la largeur du monde, vaut faux si la dimension fournie est la hauteur du monde
	 */
	public ModeleAffichage(
			final double largPixels, final double hautPixels,
			final double xOrigUnitesReelle, final double yOrigUnitesReelle,
			final double dimensionEnUnitesReelles,
			final boolean typeLargeur) {
		this.largPixels = largPixels;
		this.hautPixels = hautPixels;
		this.xOrigUnitesReelle = xOrigUnitesReelle;
		this.yOrigUnitesReelle = yOrigUnitesReelle;

		if (typeLargeur) {
			// Paramètre `dimensionEnUnitesReelles` represente la LARGEUR voulue
			largUnitesReelles = dimensionEnUnitesReelles;

			// Calcul de la hauteur correspondante pour éviter toute distortion
			hautUnitesReelles = largUnitesReelles * hautPixels / largPixels;
		} else {
			// Paramètre `dimensionEnUnitesReelles` représente la HAUTEUR au lieu de la LARGEUR
			hautUnitesReelles = dimensionEnUnitesReelles;

			// Calcul de la largeur correspondante pour éviter toute distortion
			largUnitesReelles = hautUnitesReelles * largPixels / hautPixels;
		}

		// Calcul de la matrice monde-vers-composant
		compute();
	}

	/**
	 * Création d’un objet ModeleAffichage où la dimension passée en paramètre est forcément la largeur du monde
	 *
	 * @param largPixels               La largeur du composant, en pixels
	 * @param hautPixels               La hauteur du composant, en pixels
	 * @param xOrigUnitesReelle        L’origine en x de la portion du monde réel que l’on veut montrer
	 * @param yOrigUnitesReelle        L’origine en y de la portion du monde réel que l’on veut montrer
	 * @param dimensionEnUnitesReelles La dimensions considérée dans le monde, en unité réelles (il peut s’agir d’une largeur ou d’une hauteur, dépendant du dernier paramètre)
	 */
	public ModeleAffichage(double largPixels, double hautPixels, double xOrigUnitesReelle, double yOrigUnitesReelle, double dimensionEnUnitesReelles) {
		this(largPixels, hautPixels, xOrigUnitesReelle, yOrigUnitesReelle, dimensionEnUnitesReelles, true);
	}

	/**
	 * Création d’un objet ModeleAffichage où l’origine du monde réel est à 0,0
	 *
	 * @param largPixels               La largeur du composant, en pixels
	 * @param hautPixels               La hauteur du composant, en pixels
	 * @param dimensionEnUnitesReelles La dimensions considérée dans le monde, en unité réelles (il peut s’agir d’une largeur ou d’une hauteur, dépendant du dernier paramètre)
	 * @param typeLargeur              Booléen qui vaut vrai si la dimension fournie est la largeur du monde, qui vaut faux si la dimension fournie est la hauteur du monde
	 */
	public ModeleAffichage(double largPixels, double hautPixels, double dimensionEnUnitesReelles, boolean typeLargeur) {
		this(largPixels, hautPixels, 0, 0, dimensionEnUnitesReelles, typeLargeur);
	}

	/**
	 * Création d’un objet ModeleAffichage où l’origine du monde réel est à 0,0, et où la dimension passée en paramètre est forcément la largeur du monde
	 *
	 * @param largPixels               La largeur du composant, en pixels
	 * @param hautPixels               La hauteur du composant, en pixels
	 * @param dimensionEnUnitesReelles La dimensions considérée dans le monde, en unité réelles (il peut s’agir d’une largeur ou d’une hauteur, dépendant du dernier paramètre)
	 */
	public ModeleAffichage(double largPixels, double hautPixels, double dimensionEnUnitesReelles) {
		this(largPixels, hautPixels, 0, 0, dimensionEnUnitesReelles, true);
	}

	public ModeleAffichage(final Dimension d, final double dimensionEnUnitesReelles) {
		this(d.getWidth(), d.getHeight(), dimensionEnUnitesReelles);
	}
	/**
	 * Calcul de pixels par unité X/Y, scale & translate
	 */
	private void compute() {
		pixelsParUniteX = largPixels / largUnitesReelles;
		pixelsParUniteY = hautPixels / hautUnitesReelles;

		mat.scale(pixelsParUniteX, pixelsParUniteY);
		mat.translate(-xOrigUnitesReelle, -yOrigUnitesReelle);
	}
	/**
	 * Retourne une copie de la matrice monde-vers-composant qui a été calculée dans le constructeur
	 *
	 * @return La matrice monde-vers-composant
	 */
	public AffineTransform getMat() {
		// Clone de la matrice car mutable par nature
		// TODO Pertinance
		return new AffineTransform(mat);
	}

	/**
	 * Retourne la hauteur du monde, en unités réelles
	 *
	 * @return La hauteur du monde, en unités réelles
	 */
	public double getHautUnitesReelles() {
		return hautUnitesReelles;
	}

	/**
	 * Retourne la largeur du monde, en unités réelles
	 *
	 * @return La largeur du monde, en unités réelles
	 */
	public double getLargUnitesReelles() {
		return largUnitesReelles;
	}

	/**
	 * Retourne le nombre de pixels contenus dans une unité du monde réel, en x
	 *
	 * @return Le nombre de pixels contenus dans une unité du monde réel, en x
	 */
	public double getPixelsParUniteX() {
		return pixelsParUniteX;
	}

	/**
	 * Retourne le nombre de pixels contenus dans une unité du monde réel, en y
	 *
	 * @return Le nombre de pixels contenus dans une unité du monde réel, en y
	 */
	public double getPixelsParUniteY() {
		return pixelsParUniteY;
	}

	/**
	 * Retourne la largeur en pixels du composant auquel s’appliquera la transformation
	 *
	 * @return La largeur en pixels
	 */
	public double getLargPixels() {
		return largPixels;
	}

	/**
	 * Retourne la hauteur en pixels du composant auquel s’appliquera la transformation
	 *
	 * @return La hauteur en pixels
	 */
	public double getHautPixels() {
		return hautPixels;
	}

	/**
	 * Retourne l’origine, en x, de la portion du monde réel considérée
	 *
	 * @return L’origine en x, de la portion du monde réel considérée
	 */
	public double getxOrigUnitesReelle() {
		return xOrigUnitesReelle;
	}

	/**
	 * Retourne l’origine, en y, de la portion du monde réel considérée
	 *
	 * @return L’origine en y, de la portion du monde réel considérée
	 */
	public double getyOrigUnitesReelle() {
		return yOrigUnitesReelle;
	}
}
