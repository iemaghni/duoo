package utils;

import java.util.Objects;

/**
 * La classe Vector permet de realiser les operations de base sur un Vector Euclidien en deux dimensions (x,y), où x et y sont les
 * composantes du Vector.
 * <p>
 * Cette classe est une version 2d modifiée de la classe SVecteur3d ecrite par Simon Vezina.
 * <p>
 * Efficiency over Clarity (Minimal call stack + minimal instanciation = Minimal footprint)
 *
 * @author Simon Vézina
 * @author Caroline Houle
 * @author Issam Maghni
 */
public class Vector implements java.io.Serializable
{
	// Valeur infinitésimale
	private static final double EPSILON = 1e-10;
	// Composantes scalaires
	private              double x, y;

	/**
	 * Constructeur representant un Vector 2d aux composantes nulles
	 */
	public Vector() {
		this(0, 0);
	}

	// Default constructor: new Vector().setXY();

	/**
	 * Constructeur avec composantes X &amp; Y
	 *
	 * @param x La composante X du vecteur
	 * @param y La composante Y du vecteur
	 */
	public Vector(final double x, final double y) {
		setXY(x, y);
	}

	public static void main(final String... args) {
		final Vector v = new Vector();

		// Tests for cross function
		System.out.println(v.setXY(0, 1).cross(1).equals(new Vector(1, 0)));
		System.out.println(v.setXY(0, 1).cross(-1).equals(new Vector(-1, 0)));
		System.out.println(v.setXY(1, 0).cross(1).equals(new Vector(0, -1)));
		System.out.println(v.setXY(1, 0).cross(-1).equals(new Vector(0, 1)));
		System.out.println(v.setXY(0, -1).cross(1).equals(new Vector(-1, 0)));
		System.out.println(v.setXY(0, -1).cross(-1).equals(new Vector(1, 0)));
		System.out.println(v.setXY(-1, 0).cross(1).equals(new Vector(0, 1)));
		System.out.println(v.setXY(-1, 0).cross(-1).equals(new Vector(0, -1)));
		System.out.println(v.setXY(1, 2).cross(3).equals(new Vector(6, -3)));

		System.out.println(
				Double.compare(v.setXY(1, 1).modulus(new Vector(4, 5)),
						v.setXY(4, 5).modulus(new Vector(1, 1))) == 0);

		// Tests for modulus function
		System.out.println(v.setXY(3, 4).modulus() == 5);
	}

	/**
	 * Methode qui calcule et retourner l'addition du Vector courant et d'un autre Vector. Le Vector courant reste inchangé.
	 *
	 * @param v Le Vector a ajouter au Vector courant
	 * @return La somme des deux Vecteurs
	 */
	public Vector sum(final Vector v) {
		x += v.getX();
		y += v.getY();
		return this;
	}

	/**
	 * Methode qui calcule et retourne le Vector resultant de la soustraction d'un Vector quelconque du Vector courant. Le Vector courant reste inchangé.
	 *
	 * @param v Le Vector a soustraire au Vector courant.
	 * @return La soustraction des deux Vecteurs.
	 */
	public Vector sub(final Vector v) {
		//return sum(v.clone().multiply(-1));
		x -= v.getX();
		y -= v.getY();
		return this;
	}

	/**
	 * Methode qui effectue la multiplication du Vector courant par une scalaire.Le Vector courant reste inchangé.
	 *
	 * @param m - Le muliplicateur
	 * @return Le resultat de la multiplication par un scalaire m sur le Vector.
	 */
	public Vector multiply(final double m) {
		x *= m;
		y *= m;
		return this;
	}

	/**
	 * Methode pour effectuer le produit scalaire du Vector courant avec un autre Vector.
	 *
	 * @param v L'autre Vector.
	 * @return Le produit scalaire entre les deux Vecteurs.
	 */
	public double dot(final Vector v) {
		return x * v.getX() + y * v.getY();
	}

	/**
	 * @param z Valeur réelle en Z
	 * @return Référence au vecteur lui-même
	 */
	public Vector cross(final double z) {
		return setXY(y * z, -x * z);
	}

	/**
	 * Normaliser le vecteur courant
	 *
	 * @return Référence au vecteur lui-même
	 */
	public Vector normalise() {
		//return multiply(1 / modulus());
		final double mod = modulus();
		x /= mod;
		y /= mod;
		return this;
	}

	/**
	 * Methode pour obtenir le modulus du Vector courant.
	 *
	 * @return Le modulus du vecteur courant
	 */
	public double modulus() {
		return StrictMath.hypot(x, y);
	}

	/**
	 * Former un nouveau vecteur à partir de la différence
	 * entre le vecteur courant et celui passé en paramètre,
	 * et calculer son module
	 *
	 * @param v Vecteur à soustraire
	 * @return Valeur réelle du module
	 */
	public double modulus(final Vector v) {
		//return clone().sub(v).modulus();
		return StrictMath.hypot(v.getX() - x, v.getY() - y);
	}

	/**
	 * Accesseur de la composante X du vecteur
	 *
	 * @return La composante x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Mutateur de la composante X du vecteur
	 *
	 * @param x La nouvelle composante X
	 * @return Référence au vecteur lui-même
	 */
	public Vector setX(final double x) {
		this.x = x;
		return this;
	}

	/**
	 * Accesseur de la composante Y du veteur
	 *
	 * @return Référence au vecteur lui-même
	 */
	public double getY() {
		return y;
	}

	/**
	 * Mutateur de la composante Y du vecteur
	 *
	 * @param y La nouvelle composante Y
	 * @return Référence au vecteur lui-même
	 */
	public Vector setY(final double y) {
		this.y = y;
		return this;
	}

	/**
	 * Methode qui permet de modifier les composantes du Vector.
	 *
	 * @param x La nouvelle composante X
	 * @param y La nouvelle composante Y
	 * @return Référence au vecteur lui-même
	 */
	public Vector setXY(final double x, final double y) {
		setX(x);
		setY(y);
		return this;
	}

	/**
	 * @param v Vecteur générateur
	 * @return Référence au vecteur lui-même
	 */
	public Vector set(final Vector v) {
		return setXY(v.getX(), v.getY());
	}

	/**
	 * Flush composantes scalaires
	 *
	 * @return Vecteur avec les composantes réinitialisées
	 */
	public Vector reset() {
		return setXY(0, 0);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (!(o instanceof Vector)) return false;
		final Vector v = (Vector) o;
		return x == v.getX() && y == v.getY()
				|| Math.abs(v.getX() - x) < EPSILON
				&& Math.abs(v.getY() - y) < EPSILON;
	}

	/**
	 * Générer un hash à partir des composantes du vecteur
	 *
	 * @return Hash
	 */
	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	/**
	 * Cloner le vecteur pour ne pas muter celui de base
	 *
	 * @return Vecteur cloné
	 */
	@Override
	public Vector clone() {
		return new Vector(x, y);
	}

	/**
	 * Générer une chaîne de caractères avec les informations du vecteur
	 *
	 * @return Chaîne de caractères contenant les informations du vecteur
	 */
	@Override
	public String toString() {
		return String.format("[ x = %f ; y = %f ]", x, y);
	}
}
