package sandbox;

import interfaces.Drawable;

import java.awt.*;

interface Test extends Drawable {
	// sprite “
	static void main(final String... args) {
		String fonts[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

		for (final String font : fonts)
			System.out.println(font);

		/*
		 * long l = System.currentTimeMillis(); AffineTransform mat = new
		 * AffineTransform(); mat.translate(7, 11); mat.scale(50, 50); for (int n = 0; n
		 * < 10; n++) { for (int i = 0; i < 1000000; i++) { AffineTransform matLocale =
		 * new AffineTransform(mat); matLocale.translate(13, 17); matLocale.scale(.02,
		 * .02); } System.out.println(System.currentTimeMillis() - l); }/* for (int n =
		 * 0; n < 10; n++) { for (int i = 0; i < 1000000; i++) { mat.translate(13, 17);
		 * mat.scale(.02, .02); mat.translate(-mat.getTranslateX(),
		 * -mat.getTranslateY()); mat.scale(50, 50); }
		 * System.out.println(System.currentTimeMillis() - l); }
		 */
		/*
		 * int a = 1500, b = 990, c = 990;
		 * 
		 * long ll = System.currentTimeMillis(); for (long n = 0; n < 15000000000L; n++)
		 * if (Operation.modulus(a, b) > c) continue;
		 * System.out.println(System.currentTimeMillis() - ll);
		 * 
		 * long l = System.currentTimeMillis(); for (long n = 0; n < 15000000000L; n++)
		 * if (Operation.isModulusMinor(a, b, c)) continue;
		 * System.out.println(System.currentTimeMillis() - l);
		 * 
		 * ll = System.currentTimeMillis(); for (long n = 0; n < 15000000000L; n++) if
		 * (Operation.modulus(a, b) > c) continue;
		 * System.out.println(System.currentTimeMillis() - ll);
		 * 
		 * l = System.currentTimeMillis(); for (long n = 0; n < 15000000000L; n++) if
		 * (Operation.isModulusMinor(a, b, c)) continue;
		 * System.out.println(System.currentTimeMillis() - l);
		 */
	}

	static int roundDown(final double d) {
		return (int) d;
	}

	static int roudUp(final double d) {
		return roundDown(d + 1);
	}

	default String sayHello() {
		return "hello";
	}
}
