package interfaces;

/**
 * Interface renfermant une seule méthode de dessin
 * @author Issam Maghni
 */
public interface Drawable
{
	/**
	 * Méthode de dessin
	 * @param g2d Graphic2D dans lequel le dessin se fera
	 * @param mat Matrice de dessin qui gère les proportions
	 */
	void draw(final java.awt.Graphics2D g2d, final java.awt.geom.AffineTransform mat);
}
